#include <sys/param.h>
#include <sys/conf.h>
#include <sys/kernel.h>
#include <sys/module.h>
#include <sys/systm.h>
#include <sys/uio.h>
#include <sys/malloc.h>

#define BUFFER_SIZE 256

MALLOC_DEFINE(M_ECHO_CONTIGUOUS, "echo_contiguous_buffer", "buffer for echo_contiguous driver");

static d_open_t  echo_contiguous_open;
static d_close_t echo_contiguous_close;
static d_read_t  echo_contiguous_read;
static d_write_t echo_contiguous_write;

static struct cdevsw echo_contiguous_cdevsw =
{
	.d_version = D_VERSION,
	.d_open    = echo_contiguous_open,
	.d_close   = echo_contiguous_close,
	.d_read    = echo_contiguous_read,
	.d_write   = echo_contiguous_write,
	.d_name    = "echo_contiguous"
};


typedef struct echo_contiguous_t 
{
	char buffer [ BUFFER_SIZE ];
	int  length;
} echo_contiguous_t;


static echo_contiguous_t      *echo_contiguous_message;
static struct cdev *echo_contiguous_cdev;


static int echo_contiguous_open ( struct cdev *dev, int oflags, int devtype, struct thread *td )
{
	uprintf ( "Opening echo_contiguous device!\n" );
	return  ( 0 );
}

static int echo_contiguous_close ( struct cdev *dev, int fflag, int devytpe, struct thread *td )
{
	uprintf ( "Closing echo_contiguous device!\n" );
	return  ( 0 );
}

static int echo_contiguous_write ( struct cdev *dev, struct uio *uio, int ioflag )
{
	int error = 0;

	error = copyin ( uio->uio_iov->iov_base, echo_contiguous_message->buffer, MIN ( uio->uio_iov->iov_len, BUFFER_SIZE - 1 ) );
	if ( error != 0 )
	{
		uprintf ( "Write Failed!\n" );
		return  ( error );
	}
	
	*( echo_contiguous_message->buffer + MIN ( uio->uio_iov->iov_len, BUFFER_SIZE - 1 ) ) = 0;
	echo_contiguous_message->length = MIN ( uio->uio_iov->iov_len, BUFFER_SIZE - 1 );

	return ( error );
}


static int echo_contiguous_read ( struct cdev *dev, struct uio *uio, int ioflag )
{
	int error = 0;
	int amount;
	amount = MIN ( uio->uio_resid, ( echo_contiguous_message->length - uio->uio_offset > 0 ) ? echo_contiguous_message->length - uio->uio_offset : 0 );
	error = uiomove ( echo_contiguous_message->buffer + uio->uio_offset, amount, uio );
	if ( error != 0 )
	{
		uprintf("Read failed.\n");
		return ( error );
	}
	return ( error );
}


static int echo_contiguous_modevent ( module_t mod __unused, int event, void *arg __unused )
{
	int error = 0;
	switch ( event ) 
	{
		case MOD_LOAD:
		{
			echo_contiguous_message = contigmalloc ( sizeof ( echo_contiguous_t ), M_ECHO_CONTIGUOUS, M_WAITOK | M_ZERO, 0, 0xffffffff, PAGE_SIZE, 1024 * 1024 );
			echo_contiguous_cdev = make_dev ( &echo_contiguous_cdevsw, 0, UID_ROOT, GID_WHEEL, 0600, "echo_contiguous" );
			uprintf ( "echo_contiguous driver loaded.\n" );
		} break;

		case MOD_UNLOAD:
		{
			destroy_dev ( echo_contiguous_cdev );
			contigfree ( echo_contiguous_message, sizeof ( echo_contiguous_t ), M_ECHO_CONTIGUOUS );
			uprintf ( "echo_contiguous driver unloaded.\n" );
		} break;

		default:
		{
			error = EOPNOTSUPP;
		} break;
	}

	return ( error );
}


static moduledata_t echo_contiguous_module = 
{
	"echo_contiguous",
	echo_contiguous_modevent,
	NULL
};

DECLARE_MODULE ( echo_contiguous, echo_contiguous_module, SI_SUB_DRIVERS, SI_ORDER_MIDDLE );
