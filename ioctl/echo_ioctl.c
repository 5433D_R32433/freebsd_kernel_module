#include <sys/param.h>
#include <sys/conf.h>
#include <sys/kernel.h>
#include <sys/module.h>
#include <sys/systm.h>
#include <sys/uio.h>
#include <sys/malloc.h>
#include <sys/ioccom.h>

#define BUFFER_SIZE 256

MALLOC_DEFINE(M_ECHO_IOCTL, "echo_ioctl_buffer", "buffer for echo_ioctl driver");

#define ECHO_IOCTL_CLEAR_BUFFER     _IO('E', 1)
#define ECHO_IOCTL_SET_BUFFER_SIZE _IOW('E', 2, int)

static d_open_t  echo_ioctl_open;
static d_close_t echo_ioctl_close;
static d_read_t  echo_ioctl_read;
static d_write_t echo_ioctl_write;
static d_ioctl_t echo_ioctl_ioctl;

static struct cdevsw echo_ioctl_cdevsw =
{
	.d_version = D_VERSION,
	.d_open    = echo_ioctl_open,
	.d_close   = echo_ioctl_close,
	.d_read    = echo_ioctl_read,
	.d_write   = echo_ioctl_write,
	.d_ioctl   = echo_ioctl_ioctl,
	.d_name    = "echo_ioctl"
};


typedef struct echo_ioctl_t 
{
	int   buffer_size;
	char *buffer;
	int   length;
} echo_ioctl_t;


static echo_ioctl_t      *echo_ioctl_message;
static struct cdev *echo_ioctl_cdev;


static int echo_ioctl_open ( struct cdev *dev, int oflags, int devtype, struct thread *td )
{
	uprintf ( "Opening echo_ioctl device!\n" );
	return  ( 0 );
}

static int echo_ioctl_close ( struct cdev *dev, int fflag, int devytpe, struct thread *td )
{
	uprintf ( "Closing echo_ioctl device!\n" );
	return  ( 0 );
}

static int echo_ioctl_write ( struct cdev *dev, struct uio *uio, int ioflag )
{
    int error  = 0;
    int amount = 0;
    amount = MIN ( uio->uio_resid, ( echo_ioctl_message->buffer_size - 1 - uio->uio_offset > 0 ) ? echo_ioctl_message->buffer_size - 1 - uio->uio_offset : 0 );

    if ( amount == 0 )
    {
        return ( error );
    }

    error = uiomove ( echo_ioctl_message->buffer, amount, uio );
    if ( error != 0 ) 
    {
        uprintf ( "write failed.\n" );
        return  ( error );
    }
    
    echo_ioctl_message->buffer [ amount ] = '\0';
    echo_ioctl_message->length            = amount;
    return ( error );
}

static int echo_ioctl_read ( struct cdev *dev, struct uio *uio, int ioflag )
{
    int error = 0;
	int amount;
	amount = MIN ( uio->uio_resid, ( echo_ioctl_message->length - uio->uio_offset > 0 ) ? echo_ioctl_message->length - uio->uio_offset : 0 );
	error = uiomove ( echo_ioctl_message->buffer + uio->uio_offset, amount, uio );
	if ( error != 0 )
	{
		uprintf("Read failed.\n");
		return ( error );
	}
	return ( error );
}

static int echo_ioctl_set_buffer_size ( int size )
{
    int error = 0;
    if ( echo_ioctl_message->buffer_size == size )
    {
        return ( error );
    }

    if ( size >= 128 && size <= 512 ) 
    {
        echo_ioctl_message->buffer = realloc ( echo_ioctl_message->buffer, size, M_ECHO_IOCTL, M_WAITOK );
        echo_ioctl_message->buffer_size = size;

        if ( echo_ioctl_message->length >= size ) 
        {
            echo_ioctl_message->length              = size - 1;
            echo_ioctl_message->buffer [ size - 1 ] = '\0';
        }
    } 
    else
    {
        error = EINVAL;
    }

    return (error);
}



static int echo_ioctl_ioctl ( struct cdev *dev, u_long cmd, caddr_t data, int fflag, struct thread *td )
{
	int error = 0;
	switch ( cmd ) 
	{
		case ECHO_IOCTL_CLEAR_BUFFER:
		{
			memset ( echo_ioctl_message->buffer, 0, echo_ioctl_message->buffer_size );
			echo_ioctl_message->length = 0;
			uprintf ( "Buffer cleared.\n" );
		} break;

		case ECHO_IOCTL_SET_BUFFER_SIZE:
		{
			error = echo_ioctl_set_buffer_size ( *( int* ) data );
			if ( error == 0 )
			{
				uprintf ( "Buffer resized.\n" );
			}
		} break;
		
		default:
		{
			error = ENOTTY;
		} break;
	}
	return ( error );
}

static int echo_ioctl_modevent ( module_t mod __unused, int event, void *arg __unused )
{
    int error = 0;
    switch ( event ) 
    {
        case MOD_LOAD:
        {
            echo_ioctl_message              = malloc ( sizeof ( echo_ioctl_t ), M_ECHO_IOCTL, M_WAITOK );
            echo_ioctl_message->buffer_size = 256;
            echo_ioctl_message->buffer      = malloc ( echo_ioctl_message->buffer_size, M_ECHO_IOCTL, M_WAITOK );
            echo_ioctl_cdev                 = make_dev ( &echo_ioctl_cdevsw, 0, UID_ROOT, GID_WHEEL, 0600, "echo_ioctl" );
            uprintf ( "echo_ioctl driver loaded.\n" );
        } break;

        case MOD_UNLOAD:
        {
            destroy_dev ( echo_ioctl_cdev );
            free        ( echo_ioctl_message->buffer, M_ECHO_IOCTL );
            free        ( echo_ioctl_message, M_ECHO_IOCTL );
            uprintf     ( "echo_ioctl driver unloaded.\n" );
        } break;

        default:
        {
            error = EOPNOTSUPP;
        } break;
    }

    return (error);
}


DEV_MODULE ( echo_ioctl, echo_ioctl_modevent, NULL );

#if 0
static moduledata_t echo_ioctl_module = 
{
	"echo_ioctl",
	echo_ioctl_modevent,
	NULL
};
DECLARE_MODULE ( echo_ioctl, echo_ioctl_module, SI_SUB_DRIVERS, SI_ORDER_MIDDLE );
#endif
