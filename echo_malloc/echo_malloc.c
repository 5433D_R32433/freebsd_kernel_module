#include <sys/param.h>
#include <sys/conf.h>
#include <sys/kernel.h>
#include <sys/module.h>
#include <sys/systm.h>
#include <sys/uio.h>
#include <sys/malloc.h>

#define BUFFER_SIZE 256

MALLOC_DECLARE ( M_ECHO_MALLOC );
MALLOC_DEFINE  ( M_ECHO_MALLOC, "echo_malloc_buffer", "buffer for echo_malloc driver!" ); 


static d_open_t  echo_malloc_open;
static d_close_t echo_malloc_close;
static d_read_t  echo_malloc_read;
static d_write_t echo_malloc_write;

static struct cdevsw echo_malloc_cdevsw =
{
	.d_version = D_VERSION,
	.d_open    = echo_malloc_open,
	.d_close   = echo_malloc_close,
	.d_read    = echo_malloc_read,
	.d_write   = echo_malloc_write,
	.d_name    = "echo_malloc"
};


typedef struct echo_malloc_t 
{
	char buffer [ BUFFER_SIZE ];
	int  length;
} echo_malloc_t;


static echo_malloc_t      *echo_malloc_message;
static struct cdev *echo_malloc_cdev;


static int echo_malloc_open ( struct cdev *dev, int oflags, int devtype, struct thread *td )
{
	uprintf ( "Opening echo_malloc device!\n" );
	return  ( 0 );
}

static int echo_malloc_close ( struct cdev *dev, int fflag, int devytpe, struct thread *td )
{
	uprintf ( "Closing echo_malloc device!\n" );
	return  ( 0 );
}

static int echo_malloc_write ( struct cdev *dev, struct uio *uio, int ioflag )
{
	int error = 0;

	error = copyin ( uio->uio_iov->iov_base, echo_malloc_message->buffer, MIN ( uio->uio_iov->iov_len, BUFFER_SIZE - 1 ) );
	if ( error != 0 )
	{
		uprintf ( "Write Failed!\n" );
		return  ( error );
	}
	
	*( echo_malloc_message->buffer + MIN ( uio->uio_iov->iov_len, BUFFER_SIZE - 1 ) ) = 0;
	echo_malloc_message->length = MIN ( uio->uio_iov->iov_len, BUFFER_SIZE - 1 );

	return ( error );
}


static int echo_malloc_read ( struct cdev *dev, struct uio *uio, int ioflag )
{
	int error = 0;
	int amount;
	amount = MIN ( uio->uio_resid, ( echo_malloc_message->length - uio->uio_offset > 0 ) ? echo_malloc_message->length - uio->uio_offset : 0 );
	error = uiomove ( echo_malloc_message->buffer + uio->uio_offset, amount, uio );
	if ( error != 0 )
	{
		uprintf("Read failed.\n");
		return ( error );
	}
	return ( error );
}


static int echo_malloc_modevent ( module_t mod __unused, int event, void *arg __unused )
{
	int error = 0;
	switch ( event ) 
	{
		case MOD_LOAD:
		{
			echo_malloc_message = malloc ( sizeof ( echo_malloc_t ), M_ECHO_MALLOC, M_WAITOK );
			echo_malloc_cdev = make_dev ( &echo_malloc_cdevsw, 0, UID_ROOT, GID_WHEEL, 0600, "echo_malloc" );
			uprintf ( "echo_malloc driver loaded.\n" );
		} break;

		case MOD_UNLOAD:
		{
			destroy_dev ( echo_malloc_cdev );
			free ( echo_malloc_message, M_ECHO_MALLOC );
			uprintf ( "echo_malloc driver unloaded.\n" );
		} break;

		default:
		{
			error = EOPNOTSUPP;
		} break;
	}

	return ( error );
}


static moduledata_t echo_malloc_module = 
{
	"echo_malloc",
	echo_malloc_modevent,
	NULL
};

DECLARE_MODULE ( echo_malloc, echo_malloc_module, SI_SUB_DRIVERS, SI_ORDER_MIDDLE );
