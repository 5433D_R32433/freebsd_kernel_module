#include <sys/param.h>
#include <sys/module.h>
#include <sys/kernel.h>
#include <sys/systm.h>

static int hello_modevent ( module_t mod __unused, int event, void *arg __unused )
{
    int e = 0;
    switch ( event ) 
    {
	    case MOD_LOAD:
	    {
		    uprintf ( "Kernel Module Loaded!\n" );
	    } break;

    	    case MOD_UNLOAD:
	    {
		    uprintf ( "Kernel Module Unloaded!\n" );
            } break;

    	    default:
            {
		    e = EOPNOTSUPP;
	    } break;
    }

    return ( e );
}

moduledata_t hello_mod = 
{
    "hello",                
    hello_modevent,         
    NULL             
};

DECLARE_MODULE ( hello, hello_mod, SI_SUB_DRIVERS, SI_ORDER_MIDDLE );
MODULE_VERSION ( hello, 1 );
// MODULE_DEPEND  ( hello, kernel, MIN_KVER, PREF_KVER, MAX_KVER );

